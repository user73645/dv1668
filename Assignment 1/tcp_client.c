#include <netinet/in.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>

int main(void) {
    in_addr_t inet_addr(const char *cp);
    struct sockaddr_in address;
    int close(int fildes);

    char *header = "message moment";
    char recieve_buffer[1024];
    address.sin_family = AF_INET;
    address.sin_port = htons(42069);
    address.sin_addr.s_addr = inet_addr("192.168.110.162");

    int mysocket = socket(AF_INET, SOCK_STREAM, 0);
    if (connect(mysocket, (struct sockaddr *) &address, sizeof(address))) {
        printf("Non-zero status");
        return 1;
    }

    send(mysocket, header, strlen(header), 0);
    recv(mysocket, recieve_buffer, 1024, 0);
    printf("%s", recieve_buffer);
    close(mysocket);
    return 0;
}
