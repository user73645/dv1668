#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netdb.h>
#include<netinet/in.h>

#define SA struct sockaddr // Creating Macro for the socketaddr as SA

struct sockaddr_in server, client;
char message[1440];


int main(){
   int sock = socket(AF_INET, SOCK_STREAM, 0);
   if(sock == -1){return 1;}

   server.sin_addr.s_addr = htonl(INADDR_ANY);
   server.sin_port = htons(42069);
   server.sin_family = AF_INET;
   int bind_status = bind(sock, (SA*)&server, sizeof(server));
   if(bind_status == -1){return 1;}
   int connection_status = listen(sock, 1);
   if(connection_status == -1){return 1;}
   socklen_t length = sizeof(client);
   int connection = accept(sock, (SA*)&client, &length);
   if(connection == -1){ return 1;}
   read(connection, message, sizeof(message));
   printf("Data received: %s\n", message);
   close(sock);
   return 0;
}
