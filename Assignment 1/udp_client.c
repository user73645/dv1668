#include <netinet/in.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>

int main() {

    const struct sockaddr_in* sockaddr_in;
    struct sockaddr_in address;
    int close(int fildes);
    in_addr_t inet_addr(const char *cp);

    address.sin_family = AF_INET;
    address.sin_port = htons(42069);
    address.sin_addr.s_addr = inet_addr("192.168.110.162");

    int mysocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    int status = connect(mysocket, (struct sockaddr *) &address, sizeof(address));

    if (status != 0) {printf("Non-zero status");}
    sendto(mysocket, "Very cool message", 17, 0, 0, 0);
    close(mysocket);
    return 0;
}
