#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

const struct sockaddr_in* sockaddr_in;
in_addr_t inet_addr(const char *cp);
struct sockaddr_in addr;
int close(int fildes);
/* struct timespec spec; */

int main() {
    addr.sin_family = AF_INET;
    addr.sin_port = htons(42069);
    addr.sin_addr.s_addr = inet_addr("192.168.110.162");
    /* spec.tv_nsec = 20000000; // Send every 0.02 seconds (50/s) */
    /* spec.tv_nsec = 0; */

    int soc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP),
        stat = connect(soc, (struct sockaddr *) &addr, sizeof(addr));
    char message[1440], msg_string[7]; FILE* hosts;

    if ((hosts = fopen("hosts.txt","r")) == NULL && stat != 0) {return 1;}
    for (int msg_num = 0; !feof(hosts); msg_num++) {
        snprintf(msg_string, 7, "%06d", msg_num);
        strncpy(message, msg_string, 6);

        for (int i = 6; i < 1439; i++) {message[i] = fgetc(hosts);}
        message[1439] = '$';
        sendto(soc, message, strlen(message), 0, 0, 0);
        /* nanosleep(&spec, &spec); */
    }
    fclose(hosts); close(soc);
    return 0;
}
