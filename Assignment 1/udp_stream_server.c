#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

int main(void){
	
	const int payload_size = 1440;
	const int seq_len = 6;
	const int msg_len = payload_size - seq_len - 1;
	const char end = '$';

	int socketfd;
	char payload[payload_size];
	struct sockaddr_in addr, remote_addr;
	
	socketfd = socket(AF_INET, SOCK_DGRAM, 0);
	addr.sin_family      = AF_INET;
	addr.sin_port        = htons(42069);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	
	bind(socketfd, (struct sockaddr *)&addr, sizeof(addr));
	char seq[seq_len];
	char msg[payload_size - seq_len];
	
	// File handeling
	FILE* fd = fopen("output.txt", "w");
	if (fd == NULL) {
		printf("File not found"); 
		exit(1);
	}
	
	unsigned long int packet_count = 0;
	while (1) {
		memset(&payload, 0, strlen(payload));
		recvfrom(socketfd, &payload, payload_size, 0, (struct sockaddr *)&remote_addr, 
									(unsigned int*)sizeof(remote_addr));
		strncpy(seq, payload, seq_len);
		strncpy(msg, payload + seq_len, msg_len);
		msg[msg_len] = '\0';
		
		long int seq_n = strtol(seq, NULL, 10);
		if (seq_n != packet_count + 1 ||  packet_count == 0){ 
			printf("Packet [ %s ] was recieved \n", seq);
			packet_count++;
		}else{
			printf("Packet [ %s ] is recieved out of order\n", seq);
			packet_count = seq_n + 1;
		}
		
		fflush(stdout);
		fwrite(msg, 1, sizeof(msg), fd);
	}
	fclose(fd);
	close(socketfd);
	return 0;
}
